﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class SpecialityService : ISpecialityService
    {
        public List<speciality> GetAllSpecialities()
        {
            using (var db = new AppDbContext())
            {
                var specialities = (from spe in db.speciality select spe).ToList();
                return specialities;
            }
        }

        public speciality GetSpecialityById(int id)
        {
            using (var db = new AppDbContext())
            {
                var speciality = (from spe in db.speciality where spe.id == id select spe).FirstOrDefault();
                return speciality;
            }
        }
    }
}