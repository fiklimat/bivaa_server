﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class IntervalService : IIntervalService
    {
        public interval CreateNewInterval(interval newInterval)
        {
            using (var db = new AppDbContext())
            {
                db.interval.Add(newInterval);
                db.SaveChanges();
                return newInterval;
            }
        }
        public List<interval> GetFreeFilteredIntervals(int doctor_id, int speciality_id)
        {
            using (var db = new AppDbContext())
            {
                var intervals = (from inte in db.interval where inte.reservationId == null && inte.doctorId == doctor_id && inte.specialityId == speciality_id select inte).ToList();
                return intervals;
            }
        }

        public interval GetIntervalById(int id)
        {
            using (var db = new AppDbContext())
            {
                var interval = (from inte in db.interval where inte.id == id select inte).FirstOrDefault();
                return interval;
            }
        }

        public interval GetIntervalWithReservationByReservationId(int id)
        {
            using (var db = new AppDbContext())
            {
                var intervalData = (from inte in db.interval where inte.reservationId == id select new { inte, inte.reservation }).FirstOrDefault();
                if (intervalData != null)
                    return intervalData.inte;
                else
                    return null;
            }
        }

        public List<interval> GetIntervalsWithReservationForDoctor(int id)
        {
            using (var db = new AppDbContext())
            {
                var intervalsData = (from inte in db.interval where inte.reservationId != null && inte.doctorId == id select new { inte, inte.reservation }).ToList();
                List<interval> intervals = new List<interval>();
                foreach (var interval in intervalsData)
                    intervals.Add(interval.inte);
                return intervals;
            }
        }

        public interval UpdateInterval(interval requestInterval, bool reservationIdMustBeNull)
        {
            using (var db = new AppDbContext())
            {
                var interval = GetIntervalById(requestInterval.id);
                if (interval == null)
                    return null;
                else
                {
                    db.interval.Attach(interval);
                    if (reservationIdMustBeNull && interval.reservationId != null)
                        return null;
                    else
                    {
                        interval.doctorId = requestInterval.doctorId;
                        interval.specialityId = requestInterval.specialityId;
                        interval.date = requestInterval.date;
                        interval.startTime = requestInterval.startTime;
                        interval.endTime = requestInterval.endTime;
                        interval.reservationId = requestInterval.reservationId;
                        db.SaveChanges();
                        return interval;
                    }
                }
            }
        }
        public bool DeleteInterval(int id)
        {
            using (var db = new AppDbContext())
            {
                var interval = GetIntervalById(id);
                if (interval == null)
                    return false;
                db.interval.Attach(interval);
                db.interval.Remove(interval);
                db.SaveChanges();
                return true;
            }
        }

        public void GenerateIntervals()
        {
            using (var db = new AppDbContext())
            {
                var today = DateTime.Now.Date;
                var doctorData = (from doc in db.doctor select new { doc, doc.speciality, doc.workinghours, doc.vacation, doc.interval }).ToList();
                foreach (var doctor in doctorData)
                {
                    foreach (var speciality in doctor.speciality)
                    {
                        for (DateTime date = today.AddDays(1); date < today.AddDays(15); date = date.AddDays(1))
                        {
                            Boolean onVacation = false;
                            if (doctor.vacation.Count > 0)
                            {
                                foreach (var vacation in doctor.vacation)
                                {
                                    if (date >= vacation.start && date <= vacation.end)
                                        onVacation = true;
                                }
                            }
                            if (!onVacation)
                            {
                                var workingHours = doctor.workinghours.Where(wh => wh.specialityId == speciality.id && wh.day == date.DayOfWeek.ToString()).ToList();
                                foreach (var workingHour in workingHours)
                                {
                                    for (TimeSpan time = workingHour.startTime.GetValueOrDefault(); time < workingHour.endTime; time = time.Add(new TimeSpan(0, 30, 0)))
                                    {
                                        if (!doctor.interval.Any(i => i.specialityId == speciality.id && i.date == date && i.startTime == time.ToString(@"hh\:mm")))
                                        {
                                            interval interval = new interval();
                                            interval.doctorId = doctor.doc.id;
                                            interval.specialityId = speciality.id;
                                            interval.date = date;
                                            interval.startTime = time.ToString(@"hh\:mm");
                                            interval.endTime = time.Add(new TimeSpan(0, 30, 0)).ToString(@"hh\:mm");
                                            interval.reservation = null;
                                            db.interval.Add(interval);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                db.SaveChanges();
            }
        }

        public void DeleteOldIntervals()
        {
            using (var db = new AppDbContext())
            {
                var today = DateTime.Now.Date;
                var oldIntervals = (from inte in db.interval where inte.date <= today select inte).ToList();
                var unreservedIntervals = oldIntervals.Where(inte => inte.reservationId == null).ToList();
                var reservedIntervals = oldIntervals.Where(inte => inte.reservationId != null && inte.date < today.AddDays(-8));
                ReservationService reservationService = new ReservationService();
                foreach (var interval in reservedIntervals)
                {
                    DeleteInterval(interval.id);
                    reservationService.DeleteReservation(interval.reservationId.Value);
                }
                foreach (var interval in unreservedIntervals)
                {
                    DeleteInterval(interval.id);
                }
            }
        }

        public void UpdateIntervalsIfNecessary()
        {
            using (var db = new AppDbContext())
            {
                var lastUpdate = (from up in db.updated select up).FirstOrDefault();
                var today = DateTime.Now.Date;
                if (today != lastUpdate.date)
                {
                    DeleteOldIntervals();
                    GenerateIntervals();
                    db.updated.Attach(lastUpdate);
                    lastUpdate.date = today;
                    db.SaveChanges();
                }
            }
        }
    }
}