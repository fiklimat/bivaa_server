﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class DoctorService : IDoctorService
    {
        public List<doctor> GetAllDoctors()
        {
            using (var db = new AppDbContext())
            {
                var doctorsData = (from doc in db.doctor select new {doc, doc.speciality }).ToList();
                var doctors = new List<doctor>();
                foreach (var doctorData in doctorsData)
                {
                    doctor doctor = doctorData.doc;
                    foreach (var speciality in doctorData.speciality)
                        doctor.speciality.Add(speciality);
                    doctors.Add(doctor);
                }
                return doctors;
            }
        }

        public doctor GetDoctorById(int id)
        {
            using (var db = new AppDbContext())
            {
                var doctorData = (from doc in db.doctor where doc.id == id select new { doc, doc.speciality }).FirstOrDefault();
                doctor doctor = doctorData.doc;
                foreach (var speciality in doctorData.speciality)
                    doctor.speciality.Add(speciality);
                return doctor;
            }
        }

        public doctor GetDoctorByUsername(string username)
        {
            using (var db = new AppDbContext())
            {
                var doctorData = (from doc in db.doctor where doc.username == username select new { doc, doc.speciality }).FirstOrDefault();
                doctor doctor = doctorData.doc;
                foreach (var speciality in doctorData.speciality)
                    doctor.speciality.Add(speciality);
                return doctor;
            }
        }
    }
}