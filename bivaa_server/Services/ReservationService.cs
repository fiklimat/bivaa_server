﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class ReservationService : IReservationService
    {
        public reservation CreateNewReservation(reservation newReservation)
        {
            using (var db = new AppDbContext())
            {
                db.reservation.Add(newReservation);
                db.SaveChanges();
                return newReservation;
            }
        }

        public bool DeleteReservation(int id)
        {
            reservation reservation = GetReservationById(id);
            if (reservation == null)
                return false;
            else
            {
                using (var db = new AppDbContext())
                {
                    db.reservation.Attach(reservation);
                    db.reservation.Remove(reservation);
                    db.SaveChanges();
                }
                return true;
            }
        }

        public reservation GetReservationById(int id)
        {
            using (var db = new AppDbContext())
            {
                reservation reservation = (from res in db.reservation where res.id == id select res).FirstOrDefault();
                return reservation;
            }
        }
    }
}