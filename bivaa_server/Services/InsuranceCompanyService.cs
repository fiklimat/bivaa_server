﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class InsuranceCompanyService : IInsuranceCompanyService
    {
        public List<insurancecompany> GetAllInsuranceCompanies()
        {
            using (var db = new AppDbContext())
            {
                var insuranceCompanies = (from ic in db.insurancecompany select ic).ToList();
                return insuranceCompanies;
            }
        }
    }
}