﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class VisitService : IVisitService
    {
        public List<typeofvisit> GetAllTypesOfVisit()
        {
            using (var db = new AppDbContext())
            {
                var visits = (from vis in db.typeofvisit select vis).ToList();
                return visits;
            }
        }

        public typeofvisit GetTypeOfVisitById(int id)
        {
            using (var db = new AppDbContext())
            {
                var visit = (from vis in db.typeofvisit where vis.id == id select vis).FirstOrDefault();
                return visit;
            }
        }
    }
}