﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class WorkingHoursService : IWorkingHoursService
    {
        public workinghours CreateWorkinghours(workinghours requestWorkinghours)
        {
            using (var db = new AppDbContext())
            {
                List<workinghours> allWorkinghours = (from wh in db.workinghours where wh.doctorId == requestWorkinghours.doctorId && wh.day == requestWorkinghours.day select wh).ToList();
                foreach (var workinghour in allWorkinghours)
                {
                    if ((requestWorkinghours.startTime >= workinghour.startTime && requestWorkinghours.startTime < workinghour.endTime) || (requestWorkinghours.endTime > workinghour.startTime && requestWorkinghours.endTime <= workinghour.endTime))
                        return null;
                }
                db.workinghours.Add(requestWorkinghours);
                db.SaveChanges();
                return requestWorkinghours;
            }
        }

        public bool DeleteWorkingHours(int id)
        {
            workinghours workinghours = GetWorkingHoursById(id);
            if (workinghours == null)
                return false;
            else
            {
                using (var db = new AppDbContext())
                {
                    db.workinghours.Attach(workinghours);
                    db.workinghours.Remove(workinghours);
                    db.SaveChanges();
                }
                return true;
            }
        }

        public workinghours GetWorkingHoursById(int id)
        {
            using (var db = new AppDbContext())
            {
                workinghours workinghours = (from wh in db.workinghours where wh.id == id select wh).FirstOrDefault();
                return workinghours;
            }
        }

        public List<workinghours> GetWorkingHoursForDoctorSpeciality(int doctorId, int specialityId)
        {
            using (var db = new AppDbContext())
            {
                var workingHours = (from wh in db.workinghours where wh.doctorId == doctorId && wh.specialityId == specialityId select wh).ToList();
                return workingHours;
            }
        }
    }
}