﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bivaa_server.Utils
{
    public static class Extensions
    {
        public static string GetHash(this string input)
        {
            using (var sha = new System.Security.Cryptography.SHA256Managed())
            {
                byte[] textData = System.Text.Encoding.UTF8.GetBytes(input);
                byte[] hash = sha.ComputeHash(textData);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }
        public static JObject doctorToJson(this doctor doctor)
        {
            dynamic result = new JObject();
            result.id = doctor.id;
            result.name = doctor.name;
            result.surname = doctor.surname;
            result.title = doctor.title;
            var specialityIds = new JArray();
            foreach (var speciality in doctor.speciality)
            {
                specialityIds.Add(speciality.id);
            }
            result.specialityIds = specialityIds;
            return result;
        }
        public static JArray doctorsToJson(this List<doctor> doctors)
        {
            dynamic result = new JArray();
            foreach (var doctor in doctors)
            {
                result.Add(doctor.doctorToJson());
            }
            return result;
        }
        public static JObject specialityToJson(this speciality speciality)
        {
            dynamic result = new JObject();
            result.id = speciality.id;
            result.name = speciality.name;
            return result;
        }
        public static JArray specialitiesToJson(this List<speciality> specialities)
        {
            dynamic result = new JArray();
            foreach (var speciality in specialities)
            {
                result.Add(speciality.specialityToJson());
            }
            return result;
        }
        public static JObject visitToJson(this typeofvisit typeofvisit)
        {
            dynamic result = new JObject();
            result.id = typeofvisit.id;
            result.doctorId = typeofvisit.doctorId;
            result.specialityId = typeofvisit.specialityId;
            result.name = typeofvisit.name;
            result.length = typeofvisit.length;
            return result;
        }
        public static JArray visitsToJson(this List<typeofvisit> visits)
        {
            dynamic result = new JArray();
            foreach (var visit in visits)
            {
                result.Add(visit.visitToJson());
            }
            return result;
        }
        public static JObject intervalToJson(this interval interval)
        {
            dynamic result = new JObject();
            result.id = interval.id;
            result.doctorId = interval.doctorId;
            result.specialityId = interval.specialityId;
            result.date = interval.date.ToString("yyyy/MM/dd");
            result.startTime = interval.startTime;
            result.endTime = interval.endTime;
            result.reservationId = interval.reservationId;
            return result;
        }
        public static JArray intervalsToJson(this List<interval> intervals)
        {
            dynamic result = new JArray();
            foreach (var interval in intervals)
            {
                result.Add(interval.intervalToJson());
            }
            return result;
        }
        public static JObject insuranceCompanyToJson(this insurancecompany insuranceCompany)
        {
            dynamic result = new JObject();
            result.id = insuranceCompany.id;
            result.name = insuranceCompany.name;
            result.shortname = insuranceCompany.shortname;
            return result;
        }
        public static JArray insuranceCompaniesToJson(this List<insurancecompany> insuranceCompanies)
        {
            dynamic result = new JArray();
            foreach (var insuranceCompany in insuranceCompanies)
            {
                result.Add(insuranceCompany.insuranceCompanyToJson());
            }
            return result;
        }
        public static JObject reservationToJson(this reservation reservation)
        {
            dynamic result = new JObject();
            result.id = reservation.id;
            result.typeofvisitId = reservation.typeofvisitId;
            result.patientName = reservation.patientName;
            result.patientSurname = reservation.patientSurname;
            result.email = reservation.email;
            result.phone = reservation.phone;
            result.insurancecompanyId = reservation.insurancecompanyId;
            result.note = reservation.note;
            return result;
        }
        public static JObject intervalWithReservationToJson(this interval interval)
        {
            dynamic JInterval = intervalToJson(interval);
            dynamic JReservation = reservationToJson(interval.reservation);
            JReservation.Remove("id");
            dynamic result = new JObject();
            result.Merge(JInterval);
            result.Merge(JReservation);
            return result;
        }
        public static JArray intervalsWithReservationToJson(this List<interval> intervals)
        {
            dynamic result = new JArray();
            foreach (var interval in intervals)
            {
                result.Add(interval.intervalWithReservationToJson());
            }
            return result;
        }
        public static JObject workingHourToJson(this workinghours workinghours)
        {
            dynamic result = new JObject();
            result.id = workinghours.id;
            result.doctorId = workinghours.doctorId;
            result.specialityId = workinghours.specialityId;
            result.day = workinghours.day;
            result.startTime = workinghours.startTime;
            result.endTime = workinghours.endTime;
            return result;
        }
        public static JArray workingHoursToJson(this List<workinghours> workinghours)
        {
            dynamic result = new JArray();
            foreach (var workinghour in workinghours)
            {
                result.Add(workinghour.workingHourToJson());
            }
            return result;
        }
        public static string convertToString(this Object obj)
        {
            return Convert.ToString(obj);
        }
        public static T fromJson<T>(this string obj)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(obj);
            }
            catch
            {
                return default(T);
            }
        }
        public static reservation reservationFromJson(this string request)
        {
            try
            {
                JObject obj = JObject.Parse(request);
                string jsonReservation = obj["reservation"].ToString();
                string jsonIntervalId = obj["intervalId"].ToString();
                reservation reservation = JsonConvert.DeserializeObject<reservation>(jsonReservation);
                interval interval = new interval();
                interval.id = JsonConvert.DeserializeObject<int>(jsonIntervalId);
                reservation.interval.Add(interval);
                return reservation;
            }
            catch
            {
                return null;
            }
        }

        public static void ApplyJsonContentType(this HttpResponseMessage msg, string mediaType = "application/json")
        {
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(mediaType);
        }
        public static void ApplyStatusCode(this HttpResponseMessage msg, int statusCode)
        {
            msg.StatusCode = (System.Net.HttpStatusCode)statusCode;
        }
    }
}