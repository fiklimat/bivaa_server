﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bivaa_server.Core
{
    public interface ICommonService
    {
        HttpResponseMessage GetResponse(int statusCode);
        HttpResponseMessage GetResponse(string content);
        HttpResponseMessage GetResponse(string content, int statusCode);
    }
}