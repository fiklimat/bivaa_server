﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IReservationService
    {
        reservation GetReservationById(int id);
        reservation CreateNewReservation(reservation newReservation);
        bool DeleteReservation(int id);
    }
}