﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IDoctorService
    {
        List<doctor> GetAllDoctors();
        doctor GetDoctorByUsername(string username);
        doctor GetDoctorById(int id);
    }
}