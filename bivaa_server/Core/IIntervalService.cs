﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IIntervalService
    {
        List<interval> GetFreeFilteredIntervals(int doctorId, int specialityId);
        interval GetIntervalById(int id);
        interval GetIntervalWithReservationByReservationId(int id);
        interval UpdateInterval(interval requestInterval, bool reservationIdMustBeNull);
        bool DeleteInterval(int id);
        void UpdateIntervalsIfNecessary();
        List<interval> GetIntervalsWithReservationForDoctor(int id);
    }
}