﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface ISpecialityService
    {
        List<speciality> GetAllSpecialities();
        speciality GetSpecialityById(int id);
    }
}