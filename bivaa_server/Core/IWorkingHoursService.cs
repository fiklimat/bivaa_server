﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IWorkingHoursService
    {
        List<workinghours> GetWorkingHoursForDoctorSpeciality(int doctorId, int specialityId);
        workinghours CreateWorkinghours(workinghours requestWorkingHours);
        workinghours GetWorkingHoursById(int id);
        bool DeleteWorkingHours(int id);
    }
}