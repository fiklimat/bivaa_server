using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace bivaa_server
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("name=AppDbContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<admin> admin { get; set; }
        public virtual DbSet<doctor> doctor { get; set; }
        public virtual DbSet<insurancecompany> insurancecompany { get; set; }
        public virtual DbSet<interval> interval { get; set; }
        public virtual DbSet<reservation> reservation { get; set; }
        public virtual DbSet<speciality> speciality { get; set; }
        public virtual DbSet<typeofvisit> typeofvisit { get; set; }
        public virtual DbSet<updated> updated { get; set; }
        public virtual DbSet<vacation> vacation { get; set; }
        public virtual DbSet<workinghours> workinghours { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<admin>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<admin>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .Property(e => e.surname)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .Property(e => e.title)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<doctor>()
                .HasMany(e => e.interval)
                .WithRequired(e => e.doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doctor>()
                .HasMany(e => e.workinghours)
                .WithRequired(e => e.doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doctor>()
                .HasMany(e => e.typeofvisit)
                .WithRequired(e => e.doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doctor>()
                .HasMany(e => e.vacation)
                .WithRequired(e => e.doctor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doctor>()
                .HasMany(e => e.speciality)
                .WithMany(e => e.doctor)
                .Map(m => m.ToTable("doctorspeciality").MapLeftKey("doctorId").MapRightKey("specialityId"));

            modelBuilder.Entity<insurancecompany>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<insurancecompany>()
                .Property(e => e.shortname)
                .IsUnicode(false);

            modelBuilder.Entity<insurancecompany>()
                .HasMany(e => e.reservation)
                .WithRequired(e => e.insurancecompany)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<interval>()
                .Property(e => e.startTime)
                .IsUnicode(false);

            modelBuilder.Entity<interval>()
                .Property(e => e.endTime)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.patientName)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.patientSurname)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<reservation>()
                .Property(e => e.note)
                .IsUnicode(false);

            modelBuilder.Entity<speciality>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<speciality>()
                .HasMany(e => e.interval)
                .WithRequired(e => e.speciality)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<speciality>()
                .HasMany(e => e.workinghours)
                .WithRequired(e => e.speciality)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<speciality>()
                .HasMany(e => e.typeofvisit)
                .WithRequired(e => e.speciality)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<typeofvisit>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<typeofvisit>()
                .HasMany(e => e.reservation)
                .WithRequired(e => e.typeofvisit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<workinghours>()
                .Property(e => e.day)
                .IsUnicode(false);
        }
    }
}
