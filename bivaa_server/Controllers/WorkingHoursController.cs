﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class WorkingHoursController : BaseController
    {
        private IWorkingHoursService workingHoursService;
        public WorkingHoursController(ICommonService commonService, IWorkingHoursService workingHoursService) : base(commonService)
        {
            this.workingHoursService = workingHoursService;
        }

        [Authorize]
        [HttpGet]
        [Route("api/workingHours/all/filtered/{doctorId}/{specialityId}")]
        [ActionName("filtererd")]
        public HttpResponseMessage GetAllForDoctorSpeciality(int doctorId, int specialityId)
        {
            try
            {
                var workingHours = workingHoursService.GetWorkingHoursForDoctorSpeciality(doctorId, specialityId);
                if (workingHours == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(workingHours.workingHoursToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [Authorize]
        [HttpPost]
        [ActionName("create")]
        public HttpResponseMessage Create(HttpRequestMessage req)
        {
            try
            {
                string requestBody = req.Content.ReadAsStringAsync().Result;
                workinghours requestWorkingHours = requestBody.fromJson<workinghours>();
                if (requestWorkingHours == null)
                    return commonService.GetResponse(400);
                else if (requestWorkingHours.endTime <= requestWorkingHours.startTime)
                    return commonService.GetResponse(400);
                workinghours workingHours = workingHoursService.CreateWorkinghours(requestWorkingHours);
                if (workingHours == null)
                    return commonService.GetResponse(409);
                else
                    return commonService.GetResponse(workingHours.workingHourToJson().convertToString(), 201);
            }
            catch
            {
                return commonService.GetResponse(500);
            }

        }

        [Authorize]
        [HttpDelete]
        [Route("api/workingHours/delete/{id}")]
        [ActionName("delete")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                bool result = workingHoursService.DeleteWorkingHours(id);
                if (result == false)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(200);
            }

            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}