﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class VisitController : BaseController
    {
        private IVisitService visitService;
        public VisitController(ICommonService commonService, IVisitService visitService) : base(commonService)
        {
            this.visitService = visitService;
        }

        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var visits = visitService.GetAllTypesOfVisit();
                if (visits == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(visits.visitsToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [HttpGet]
        [Route("api/visit/detail/{id}")]
        public HttpResponseMessage GetDetail(int id)
        {
            try
            {
                var visit = visitService.GetTypeOfVisitById(id);
                if (visit == null)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(visit.visitToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}