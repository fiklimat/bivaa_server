﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class InsuranceCompanyController : BaseController
    {
        private IInsuranceCompanyService insuranceCompanyService;
        public InsuranceCompanyController(ICommonService commonService, IInsuranceCompanyService insuranceCompanyService) : base(commonService)
        {
            this.insuranceCompanyService = insuranceCompanyService;
        }

        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                List<insurancecompany> insuranceCompanies = insuranceCompanyService.GetAllInsuranceCompanies();
                if (insuranceCompanies == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(insuranceCompanies.insuranceCompaniesToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}