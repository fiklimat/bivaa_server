﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class SpecialityController : BaseController
    {
        private ISpecialityService specialityService;
        public SpecialityController(ICommonService commonService, ISpecialityService specialityService) : base(commonService)
        {
            this.specialityService = specialityService;
        }

        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var specialities = specialityService.GetAllSpecialities();
                if (specialities == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(specialities.specialitiesToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [HttpGet]
        [Route("api/speciality/detail/{id}")]
        [ActionName("detail")]
        public HttpResponseMessage GetDetail(int id)
        {
            try
            {
                var speciality = specialityService.GetSpecialityById(id);
                if (speciality == null)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(speciality.specialityToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}