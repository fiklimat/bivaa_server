﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class ReservationController : BaseController
    {
        private IReservationService reservationService;
        private IIntervalService intervalService;
        public ReservationController(ICommonService commonService, IReservationService reservationService, IIntervalService intervalService) : base(commonService)
        {
            this.reservationService = reservationService;
            this.intervalService = intervalService;
        }

        [HttpPost]
        [ActionName("create")]
        public HttpResponseMessage Create(HttpRequestMessage req)
        {
            try
            {
                string requestBody = req.Content.ReadAsStringAsync().Result;
                reservation requestReservation = requestBody.reservationFromJson();
                if (requestReservation == null)
                {
                    return commonService.GetResponse(400);
                }
                else
                {
                    interval interval = intervalService.GetIntervalById(requestReservation.interval.FirstOrDefault<interval>().id);
                    if (interval != null)
                    {
                        if (interval.reservationId == null)
                        {
                            requestReservation.interval = null;
                            reservation reservation = reservationService.CreateNewReservation(requestReservation);
                            if (reservation == null)
                                return commonService.GetResponse(500);
                            else
                            {
                                interval.reservationId = reservation.id;
                                interval updatedInterval = intervalService.UpdateInterval(interval, true);
                                if (updatedInterval != null)
                                    return commonService.GetResponse(reservation.reservationToJson().convertToString(), 201);
                                else
                                    reservationService.DeleteReservation(reservation.id);
                                return commonService.GetResponse(409);
                            }
                        }
                        else
                            return commonService.GetResponse(409);
                    }
                    else
                        return commonService.GetResponse(404);
                }
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [HttpDelete]
        [Route("api/reservation/delete/{id}")]
        [ActionName("delete")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                interval interval = intervalService.GetIntervalWithReservationByReservationId(id);
                if (interval != null)
                {
                    interval.reservationId = null;
                    if (intervalService.UpdateInterval(interval, false) == null)
                        return commonService.GetResponse(404);
                    else
                    {
                        if (!reservationService.DeleteReservation(id))
                            return commonService.GetResponse(404);
                        return commonService.GetResponse(200);
                    }
                }
                else
                    return commonService.GetResponse(404);
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/reservation/all/filtered/{doctorId}")]
        [ActionName("filtered")]
        public HttpResponseMessage GetFilteredReservations(int doctorId)
        {
            try
            {
                List<interval> filteredReservations = intervalService.GetIntervalsWithReservationForDoctor(doctorId);
                if (filteredReservations == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(filteredReservations.intervalsWithReservationToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}