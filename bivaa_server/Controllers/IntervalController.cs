﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class IntervalController : BaseController
    {
        private IIntervalService intervalService;
        public IntervalController(ICommonService commonService, IIntervalService intervalService) : base(commonService)
        {
            this.intervalService = intervalService;
        }

        [HttpGet]
        [Route("api/interval/all/free/{doctorId}/{specialityId}")]
        public HttpResponseMessage GetFreeFilteredIntervals(int doctorId, int specialityId)
        {
            try
            {
                intervalService.UpdateIntervalsIfNecessary();
            }
            catch
            {
                return commonService.GetResponse(500);
            }
            try
            {
                List<interval> intervals = intervalService.GetFreeFilteredIntervals(doctorId, specialityId);
                if (intervals == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(intervals.intervalsToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [HttpGet]
        [Route("api/interval/detail/free/{intervalId}")]
        public HttpResponseMessage GetFreeFilteredIntervalbyId(int intervalId)
        {
            try
            {
                interval interval = intervalService.GetIntervalById(intervalId);
                if (interval.reservationId != null)
                    return commonService.GetResponse(409);
                return commonService.GetResponse(interval.intervalToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}