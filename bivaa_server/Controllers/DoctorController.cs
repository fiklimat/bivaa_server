﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class DoctorController : BaseController
    {
        private IDoctorService doctorService;
        public DoctorController(ICommonService commonService, IDoctorService doctorService) : base(commonService)
        {
            this.doctorService = doctorService;
        }

        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var doctors = doctorService.GetAllDoctors();
                if (doctors == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(doctors.doctorsToJson().convertToString());
            }
            catch { return commonService.GetResponse(500); }
        }

        [Authorize]
        [HttpGet]
        [Route("api/doctor/detail/username/{username}")]
        public HttpResponseMessage GetDetailByUsername(string username)
        {
            try
            {
                var doctor = doctorService.GetDoctorByUsername(username);
                if (doctor == null)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(doctor.doctorToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }

        [HttpGet]
        [Route("api/doctor/detail/{id}")]
        public HttpResponseMessage GetDetail(int id)
        {
            try
            {
                var doctor = doctorService.GetDoctorById(id);
                if (doctor == null)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(doctor.doctorToJson().convertToString());
            }
            catch
            {
                return commonService.GetResponse(500);
            }
        }
    }
}