[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(bivaa_server.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(bivaa_server.App_Start.NinjectWebCommon), "Stop")]

namespace bivaa_server.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;
    using bivaa_server.Core;
    using bivaa_server.Services;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using Ninject.Web.WebApi;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application.
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICommonService>().To<CommonService>();
            kernel.Bind<CommonService>().To<CommonService>().InSingletonScope();

            kernel.Bind<IDoctorService>().To<DoctorService>();
            kernel.Bind<DoctorService>().To<DoctorService>().InSingletonScope();

            kernel.Bind<ISpecialityService>().To<SpecialityService>();
            kernel.Bind<SpecialityService>().To<SpecialityService>().InSingletonScope();

            kernel.Bind<IVisitService>().To<VisitService>();
            kernel.Bind<VisitService>().To<VisitService>().InSingletonScope();

            kernel.Bind<IIntervalService>().To<IntervalService>();
            kernel.Bind<IntervalService>().To<IntervalService>().InSingletonScope();

            kernel.Bind<IInsuranceCompanyService>().To<InsuranceCompanyService>();
            kernel.Bind<InsuranceCompanyService>().To<InsuranceCompanyService>().InSingletonScope();

            kernel.Bind<IReservationService>().To<ReservationService>();
            kernel.Bind<ReservationService>().To<ReservationService>().InSingletonScope();

            kernel.Bind<IWorkingHoursService>().To<WorkingHoursService>();
            kernel.Bind<WorkingHoursService>().To<WorkingHoursService>().InSingletonScope();
        }
    }
}