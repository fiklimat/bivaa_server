namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("workinghours")]
    public partial class workinghours
    {
        public int id { get; set; }

        public int doctorId { get; set; }

        public int specialityId { get; set; }

        [StringLength(255)]
        public string day { get; set; }

        public TimeSpan? startTime { get; set; }

        public TimeSpan? endTime { get; set; }

        public virtual doctor doctor { get; set; }

        public virtual speciality speciality { get; set; }
    }
}
