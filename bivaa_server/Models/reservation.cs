namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("reservation")]
    public partial class reservation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public reservation()
        {
            interval = new HashSet<interval>();
        }

        public int id { get; set; }

        public int typeofvisitId { get; set; }

        [Required]
        [StringLength(255)]
        public string patientName { get; set; }

        [Required]
        [StringLength(255)]
        public string patientSurname { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string phone { get; set; }

        public int insurancecompanyId { get; set; }

        [StringLength(255)]
        public string note { get; set; }

        public virtual insurancecompany insurancecompany { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<interval> interval { get; set; }

        public virtual typeofvisit typeofvisit { get; set; }
    }
}
