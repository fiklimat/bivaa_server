namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("interval")]
    public partial class interval
    {
        public int id { get; set; }

        public int doctorId { get; set; }

        public int specialityId { get; set; }

        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        [Required]
        [StringLength(255)]
        public string startTime { get; set; }

        [Required]
        [StringLength(255)]
        public string endTime { get; set; }

        public int? reservationId { get; set; }

        public virtual doctor doctor { get; set; }

        public virtual reservation reservation { get; set; }

        public virtual speciality speciality { get; set; }
    }
}
