namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("typeofvisit")]
    public partial class typeofvisit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public typeofvisit()
        {
            reservation = new HashSet<reservation>();
        }

        public int id { get; set; }

        public int doctorId { get; set; }

        public int specialityId { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        public int length { get; set; }

        public virtual doctor doctor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reservation> reservation { get; set; }

        public virtual speciality speciality { get; set; }
    }
}
